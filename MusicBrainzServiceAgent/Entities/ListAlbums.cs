﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicBrainzServiceAgent.Entities
{
    public class ListAlbums
    {
        public List<Album> Albums { get; set; }
    }

    public class Album
    {
        public string title { get; set; }
    }
}
