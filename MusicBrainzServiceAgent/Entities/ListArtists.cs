﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicBrainzServiceAgent.Entities
{
    public class ListArtists
    {
        public List<Artist> Artists { get; set; }
    }

    public class Artist
    {
        public string name { get; set; }
        public string country { get; set; }
    }
}
