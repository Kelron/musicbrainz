﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using MusicBrainzServiceAgent.Entities;

namespace MusicBrainzServiceAgent
{
    public class ServiceAgent
    {

        const string baseUrl = "https://musicbrainz.org/ws/2/";

        public T Execute<T>(RestRequest request) where T : new()
        {
            var client = new RestClient
            {
                BaseUrl = new System.Uri(baseUrl)
            };

            var response = client.Execute<T>(request);

            if(response.ErrorException != null)
            {
                const string message = "Error retrieving response. Check inner details for more info.";
                var musicBrainzException = new ApplicationException(message, response.ErrorException);
                throw musicBrainzException;
            }
            return response.Data;
        }


        public ListArtists GetArtists(string artist)
        {
            var request = new RestRequest
            {
                Resource = "artist/?query=artist:{artist}",
                RootElement = "artist-list"
            };

            request.AddHeader("User-Agent", "Sample for student");

            request.AddParameter("artist", artist, ParameterType.UrlSegment);

            return Execute<ListArtists>(request);
        }

        public ListAlbums GetAlbums(string artist)
        {
            var request = new RestRequest
            {
                Resource = "release/?query=artist:{artist}",
                RootElement = "release-list"
            };

            request.AddHeader("User-Agent", "Sample for student");

            request.AddParameter("artist", artist, ParameterType.UrlSegment);

            return Execute<ListAlbums>(request);
        }

    }
}
