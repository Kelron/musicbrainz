﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MusicBrainzServiceAgent;

namespace TestMusicBrainzConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("--- Test Begin ---\n");

            // Recherche d'artistes
            //ServiceAgent agent = new ServiceAgent();
            //foreach (var artist in agent.GetArtists("Unleash the archers").Artists)
            //{
            //    var country = "N.C.";
            //    if (artist.country != null)
            //    {
            //        country = artist.country.ToString();
            //    }

            //    Console.WriteLine($"{artist.name.ToString()}, {country}");
            //}


            // Recherche d'albums pour un artiste
            ServiceAgent agent = new ServiceAgent();
            foreach (var album in agent.GetAlbums("Muse").Albums)
            {
                Console.WriteLine($"{album.title.ToString()}");
            }

            Console.WriteLine("\n--- Test Ended ---");
            Console.ReadLine();
        }
    }
}
